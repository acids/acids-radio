# ACIDS Radio

This repository contains all cool examples generated by projects from the
ACIDS team

## Listing

Here is the list of examples obtained

### Fabrique des monstres

Audio clips and results from the project with Daniele Ghisi for the opera
"La fabrique des monstres" by Peyret. This folder contains both training examples 
(and evolutions) along with final cuts made by Daniele.

Model used is a 3-tier SampleRNN with lots of hyperparameters tweaking. 
Leopold also coded some conditioning of the original model.

### Timbre transfer

The directory contains audio examples of timbre transfer using either UNIT or convVAE models trained on NSGT-mel spectrograms from SOL-ordinario.
Both models are partly convolutional processing chunks representing about 125ms context.
They can allow conditioning on pitch classes and/or instruments classes.
And/or UNIT models can be trained with an additional classification objective on those classes.

Here, chunks in the input space are of size 16 frames x 500 bins and encoded onto latent vectors of dimension 3 to 20.

Audio clips of 5 seconds are named as follow, [model]\_[train/eval]\_[sample\_id]\_[input\_inst-note]\_[in/gen/trans_inst].wav
eg. UNIT\_eval\_1\_ASax-ord-B4-mf\_in.wav - UNIT\_eval\_1\_ASax-ord-B4-mf\_gen.wav - UNIT\_eval\_1\_ASax-ord-B4-mf\_trans_violin.wav
note: some are rather good in term of reconstruction and "qualitative" transfer ; some are more funky ;-)

Also, with explicit names (for more simplicity) are attached some demo clips of chained transfers through matching pairs of UNIT models.